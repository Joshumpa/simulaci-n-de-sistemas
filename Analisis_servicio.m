clear

load('observaciones_servicio.mat');

pd = fitdist(o, 'loglogistic')

[h, p, stats] = chi2gof(o, 'CDF', pd)

amp = (max(o) - min(o))/9

chi2inv(0.95, 5)

% A = 14.1327;
% B = 2.22865;
% 
% [x, y] = hist(o,8);
% amp = (max(o) - min(o))/8;
% 
% minO = min(o);
% xx = zeros(1,8);
% 
% xx(1) = (wblcdf(minO, A, B) - wblcdf(minO + amp, A, B)) * 200;
% xx(2) = (wblcdf(minO + amp, A, B) - wblcdf(minO + (amp*2), A, B)) * 200;
% xx(3) = (wblcdf(minO + (amp*2), A, B) - wblcdf(minO + (amp*3), A, B)) * 200;
% xx(4) = (wblcdf(minO + (amp*3), A, B) - wblcdf(minO + (amp*4), A, B)) * 200;
% xx(5) = (wblcdf(minO + (amp*4), A, B) - wblcdf(minO + (amp*5), A, B)) * 200;
% xx(6) = (wblcdf(minO + (amp*5), A, B) - wblcdf(minO + (amp*6), A, B)) * 200;
% xx(7) = (wblcdf(minO + (amp*6), A, B) - wblcdf(minO + (amp*7), A, B)) * 200;
% xx(8) = (wblcdf(minO + (amp*7), A, B) - wblcdf(minO + (amp*8), A, B)) * 200;
% 
% xx = xx * -1;