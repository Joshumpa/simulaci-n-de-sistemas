Simulación de Sistemas

Una cola es una línea de espera y la teoría de colas es una colección de modelos
matemáticos que describen sistemas de línea de espera.
Los modelos sirven para encontrar un buen compromiso entre costos del sistema y
los tiempos promedio de la línea de espera para un sistema dado. 
El problema es determinar qué capacidad o tasa de servicio proporciona el
balance correcto. Esto no es sencillo, ya que un cliente no llega a un horario
fijo. No se sabe con exactitud en que momento llegarán los clientes. El tiempo
de servicio no tiene un tiempo prestablecido o fijo.

Algunos temas descritos en el documento son:

    - Recopilación de los datos de la empresa
        - Datos de Llegada(llegadas al sistema, llegadas sucesivas al sistema)
        - Datos de Servicio(tiempos de servicio al cliente)
    - Graficas de la información
    - Hipótesis establecidas
    - Tablas de frecuencias observadas y esperadas(Matlab)
    - Pruebas de bondad de ajuste χ2(Matlab)
    - Simulación del sistema en software Promodel

